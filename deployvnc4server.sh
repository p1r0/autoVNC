#!/bin/bash

vncConf(){
	echo "#following two lines for normal desktop:
	unset SESSION_MANAGER
	unset DBUS_SESSION_BUS_ADDRESS
	#exec /etc/X11/xinit/xinitrc
	#. /etc/X11/xinit/xinitrc
	#gnome-session  --session=ubuntu-2d &
	$2-session &
	[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
	[ -r \$HOME/.Xresources ] && xrdb \$HOME/.Xresources
	xsetroot -solid grey
	vncconfig -iconic &
	#x-terminal-emulator -geometry 80x24+10+10 -ls -title \"\$VNCDESKTOP Desktop\" &
	#x-window-manager &" > $1/xstartup
    echo "VNC configuration ready!"
    return 0
}
runner(){
	echo "#!/bin/bash
	vncserver -localhost no -password=$configRoute/vncpasswd
	" > $1/startvncserver.sh
	chmod u+x $1/startvncserver.sh
	echo "run script generated: $1/startvncserver.sh"
}

echo "this short script helps you to set a vnc4server with/out desktop"

read -p "First, give me a username:" uname

uhome=/home/$uname
echo "Default home is $uhome"
configRoute=$uhome/.vnc
mkdir $configRoute
vncpasswd $configRoute/vncpasswd
chown -Rv $uname: $configRoute/

echo "configure a vnc server"
echo "1. vncserver with xfce4 basic"
echo "2. vncserver with xfce4 full"
echo "3. vncserver with cinnamon"
echo "4. just vncserver"
echo "else, exit"

read -p "select a option (number):" choice
case "$choice" in
	1 | 1.) apt-get -y install vnc4server xfce4;
	       	vncConf $configRoute 'xfce4';
		chown -Rv $uname: $configRoute/
		runner $uhome
		chown -Rv $uname: $uhome/startvncserver.sh
		;;
	2 | 2.) apt-get -y install vnc4server xfce4  xfce4-goodies task-xfce-desktop
	       	vncConf $configRoute 'xfce4';
		chown -Rv $uname: $configRoute/
		runner $uhome
		chown -Rv $uname: $uhome/startvncserver.sh
		;;
	3 | 3.) apt-get -y install vnc4server cinnamon	
	       	vncConf $configRoute 'cinnamon';
		chown -Rv $uname: $configRoute/
		runner $uhome
		chown -Rv $uname: $uhome/startvncserver.sh
		;;
	4 | 4.) apt-get -y install vnc4server;;
	
	*) echo "no option selected";;
esac
echo "Bye ;) $uname"
